selfdoc:
	@echo "make gui"
	@echo "make run"

check_seq:
	@if [ ! -f sequoia.deep.conll ]; then echo "MISSING sequoia.deep.conll. Add a symbolic link to it"; exit 1; fi;

gui: check_seq
	grew -grs grs/dmrs_main.grs -i sequoia.deep.conll

run: check_seq
	grew -det -grs grs/dmrs_main.grs -i sequoia.deep.conll -f sequoia.dmrs.conllu

.PHONY: selfdoc check_seq gui run