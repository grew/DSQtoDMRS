%===============================================================================================
% Transferring of the target for some relations EQ.
package eq_transfer{

  rule eq{
    pattern{
      eq_rel: GOV -[EQ]-> DEP1;
      DEP1 -[EQ]-> DEP2}
    without{DEP1 -[EQ]-> GOV}
    without{GOV -[EQ]-> DEP2}
    commands{del_edge eq_rel; add_edge GOV -[EQ]-> DEP2}}

  rule prep{
    pattern{
      PREP[cat=P]; eq_rel: GOV -[EQ]-> PREP;
      PREP -[ARG1]-> ARG1}
    without{GOV -[EQ]-> ARG1}
    commands{del_edge eq_rel; add_edge GOV -[EQ]-> ARG1}}

  rule eq_loc{
    pattern{eqloc_rel: GOV -[EQ.loc]-> DEP}
    commands{del_edge eqloc_rel; add_edge GOV -[EQ]-> DEP}}

  rule arg1_loc{
    pattern{arg1loc_rel: GOV -[ARG1.loc]-> DEP}
    commands{del_edge arg1loc_rel; add_edge GOV -[ARG1]-> DEP}}

}

%===============================================================================================
% Merging of semantic roles with scope relations.
package rel_merge{

  rule arg1_eq{
    pattern{
      role_rel: X -[ARG1]-> Y;
      scope_rel: X -[EQ]-> Y}
    commands{
      del_edge role_rel;  del_edge scope_rel;
      add_edge X -[ARG1-EQ]-> Y}}

  rule arg2_eq{
    pattern{
      role_rel: X -[ARG2]-> Y;
      scope_rel: X -[EQ]-> Y}
    commands{
      del_edge role_rel;  del_edge scope_rel;
      add_edge X -[ARG2-EQ]-> Y}}

  rule arg3_eq{
    pattern{
      role_rel: X -[ARG3]-> Y;
      scope_rel: X -[EQ]-> Y}
    commands{
      del_edge role_rel;  del_edge scope_rel;
      add_edge X -[ARG3-EQ]-> Y}}

  rule arg1_h{
    pattern{
      role_rel: X -[ARG1]-> Y;
      scope_rel: X -[H]-> Y}
    commands{
      del_edge role_rel; del_edge scope_rel;
      add_edge X -[ARG1-H]-> Y}}

  rule body_h{
    pattern{
      role_rel: X -[BODY]-> Y;
      scope_rel: X -[H]-> Y}
    commands{
      del_edge role_rel;del_edge scope_rel;
      add_edge X -[BODY-H]-> Y}}

  rule rstr_h{
    pattern{
      role_rel: X -[RSTR]-> Y;
      scope_rel: X -[H]-> Y}
    commands{
      del_edge role_rel;del_edge scope_rel;
      add_edge X -[RSTR-H]-> Y}}

}
%===============================================================================================
% Deletion of nodes that have no existence at the semantic level.
package clear{

  %rule del_gramword{pattern{C[void=y]}commands{del_node C}}
  rule del_punct{pattern{P[cat=PONCT]}without{P -[ARG1]-> *}commands{del_node P}}
  rule del_modinc{pattern{modinc: GOV -[mod.inc]-> DEP} commands{del_edge modinc}}
}

% =============================================================================================
% Mark the remaining syntactic dependencies that are ignored in the conversion
package remain {

  rule dep {pattern {e:N -[dep]-> M} commands {del_edge e; add_edge N -[REMAIN_dep]->M} }
  rule mod {pattern {e:N -[mod]-> M} commands {del_edge e; add_edge N -[REMAIN_mod]->M} }
  rule modapp {pattern {e:N -[mod.app]-> M} commands {del_edge e; add_edge N -[REMAIN_mod.app]->M} }
  rule modinc {pattern {e:N -[mod.inc]-> M} commands {del_edge e; add_edge N -[REMAIN_mod.inc]->M} }
  rule modvoc {pattern {e:N -[mod.voc]-> M} commands {del_edge e; add_edge N -[REMAIN_mod.voc]->M} }

}

% =============================================================================================
% Mark the remaining syntactic dependencies
package fail {
  rule arg {pattern {e:N -[arg]-> M} commands {del_edge e; add_edge N -[FAIL_arg]->M} }
  rule argc {pattern {e:N -[argc]-> M} commands {del_edge e; add_edge N -[FAIL_argc]->M} }
  rule argcomp {pattern {e:N -[arg.comp]-> M} commands {del_edge e; add_edge N -[FAIL_arg.comp]->M} }
  rule argcons {pattern {e:N -[arg.cons]-> M} commands {del_edge e; add_edge N -[FAIL_arg.cons]->M} }
  rule ato {pattern {e:N -[ato]-> M} commands {del_edge e; add_edge N -[FAIL_ato]->M} }
  rule ats {pattern {e:N -[ats]-> M} commands {del_edge e; add_edge N -[FAIL_ats]->M} }
  rule aobj {pattern {e:N -[a_obj]-> M} commands {del_edge e; add_edge N -[FAIL_a_obj]->M} }
  rule coord {pattern {e:N -[coord]-> M} commands {del_edge e; add_edge N -[FAIL_coord]->M} }
  rule dep {pattern {e:N -[dep]-> M} commands {del_edge e; add_edge N -[FAIL_dep]->M} }
  rule depcpd {pattern {e:N -[dep_cpd]-> M} commands {del_edge e; add_edge N -[FAIL_dep_cpd]->M} }
  rule depcoord {pattern {e:N -[dep.coord]-> M} commands {del_edge e; add_edge N -[FAIL_dep.coord]->M} }
  rule depde {pattern {e:N -[dep.de]-> M} commands {del_edge e; add_edge N -[FAIL_dep.de]->M} }
  rule det {pattern {e:N -[det]-> M} commands {del_edge e; add_edge N -[FAIL_det]->M} }
  rule de_obj {pattern {e:N -[de_obj]-> M} commands {del_edge e; add_edge N -[FAIL_de_obj]->M} }
  rule dis {pattern {e:N -[dis]-> M} commands {del_edge e; add_edge N -[FAIL_dis]->M} }
  rule mod {pattern {e:N -[mod]-> M} commands {del_edge e; add_edge N -[FAIL_mod]->M} }
  rule modcomp {pattern {e:N -[mod.comp]-> M} commands {del_edge e; add_edge N -[FAIL_mod.comp]->M} }
  rule mod_rel {pattern {e:N -[mod.rel]-> M} commands {del_edge e; add_edge N -[FAIL_mod.rel]->M} }
  rule modsuper {pattern {e:N -[mod.super]-> M} commands {del_edge e; add_edge N -[FAIL_mod.super]->M} }
  rule obj {pattern {e:N -[obj]-> M} commands {del_edge e; add_edge N -[FAIL_obj]->M} }
  rule objp {pattern {e:N -[obj.p]-> M} commands {del_edge e; add_edge N -[FAIL_obj.p]->M} }
  rule objcpl {pattern {e:N -[obj.cpl]-> M} commands {del_edge e; add_edge N -[FAIL_obj.cpl]->M} }
  rule pobjagt {pattern {e:N -[p_obj.agt]-> M} commands {del_edge e; add_edge N -[FAIL_p_obj.agt]->M} }
  rule pobjo {pattern {e:N -[p_obj.o]-> M} commands {del_edge e; add_edge N -[FAIL_p_obj.o]->M} }
  rule suj {pattern {e:N -[suj]-> M} commands {del_edge e; add_edge N -[FAIL_suj]->M} }

}
