% =============================================================================================
%  Initialisation packages
% =============================================================================================
include "init.grs"

% =============================================================================================
%  Packages introducing core roles
% =============================================================================================
include "pred_roles.grs"

% =============================================================================================
%  Packages introducing non core roles
% =============================================================================================
include "modif.grs"

% =============================================================================================
%  Packages introducing specific relations
% =============================================================================================
include "spec_rel.grs"

% =============================================================================================
%  Finalisation packages
% =============================================================================================
include "final.grs"


% ===============================================================================================

% ===============================================================================================

strat prepare {
  Seq (
    Onf (void_init), % delete void dependencies that link void nodes to the root
    Onf (sequoia_amalgam), % normalize some specific sequoia annotations
    Onf (sequoia_constr),
    Onf (sequoia_enum),
    Onf (coord_fact), % factorization in conjuncts of a coordination
    Onf (mwe),% reduce multi-word expressions
    Onf (word_type), % type different kinds of words
    Onf (adj_adv), % add subjects to adverbs considered as adjectives
    Onf (adj_noun), % add subjects to nouns considered as adjectives
    Onf (adj_prep), % add subjects to prepositions considered as adjectives
    Onf (light_verb), % group the light verbs with their object in a unique semantic entity
  )
}

strat roles {
  Seq (
    Onf (rising_verb),
    Onf (copula),
    Onf (verb_role2),
    Onf (verb_role1),
    Onf (adj_role),
    Onf (deflt_role), % default roles for verbs and adjectives
    Onf (noun_role2),
    Onf (noun_role1),
  )
}

strat modifiers {
  Seq (
    Onf (negation),
    Onf (modif),
    Onf (mod_rel),  % relative clauses
    Onf (date),
  )
}

strat specific_relations {
  Seq (
    Onf(de_noun_a_noun), % constructions (de + noun + à + noun)
    Onf(link_obj),       % objects of prepostions and conjunctions of subordination
    Onf(compar),         % comparatives
    Onf(cons),           % consecutive constructions
    Onf(det),            % determiners
    Onf(mark_mod_loc),   % in coordinations
    Onf(coord),          % Ex: annodis 6 (before modif) and annodis 124, see also annodis 5 - splitting of rules for prepositions and subordinating conjonctions
  )
}

strat finalization {
  Seq (
    Onf(eq_transfer),
    Onf(rel_merge), % merge semantic roles with scope relations.
    Onf(clear),
    Onf(remain),
    Onf(fail),
  )
}

strat main {
  Seq (
    prepare,
    roles,
    modifiers,
    specific_relations,
    finalization,
  )
}

% ===============================================================================================
% strategy used for the online demo
strat demo { Seq (main, Onf (clean)) }

package clean {
  rule void { % remove nodes with void=y
    pattern { N[void=y] }
    commands { del_node N }
  }
}





